const fs = require('fs');

const TITLES_FILE = 'storage/titles.json';

if (!fs.existsSync(TITLES_FILE)) {
  if (!fs.existsSync('storage')) {
    fs.mkdirSync('storage');
  }
  fs.writeFileSync(TITLES_FILE, '[]');
}
const videoTitles = JSON.parse(fs.readFileSync(TITLES_FILE));

const sendVideoTitles = (req, res) => {
  res.send(videoTitles);
};

const uploadVideo = (req, res) => {
  const newVideo = {
    title: req.body.title,
    id: req.file.filename,
  };
  videoTitles.push(newVideo);
  fs.writeFileSync(TITLES_FILE, JSON.stringify(videoTitles));

  return res.status(200).send(res.ok);
};

module.exports.sendVideoTitles = sendVideoTitles;
module.exports.uploadVideo = uploadVideo;
