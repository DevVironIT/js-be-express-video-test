const multer = require('multer');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

const UPLOADS_FOLDER = 'public/uploads';

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (!fs.existsSync(UPLOADS_FOLDER)) {
      fs.mkdirSync(UPLOADS_FOLDER);
    }
    cb(null, UPLOADS_FOLDER)
  },
  filename: function (req, file, cb) {
    const extension = file.originalname.split('.')[1];
    const id = uuidv4();
    cb(null, `${id}.${extension}`);
  },
});

const upload = multer({ storage }).single('video');

module.exports.upload = upload;
