const {upload} = require("../middlewares/multer");
const express = require('express');
const router = express.Router();
const videoController = require('../controllers/videoController');

router.get('/', videoController.sendVideoTitles);
router.post('/', upload, videoController.uploadVideo);

module.exports = router;
